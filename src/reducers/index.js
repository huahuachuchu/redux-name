import {combineReducers} from "redux";
import nameReducer from "./name";

const reducers = combineReducers({
  nameReducer
});

export default reducers;